<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'firstname' => true,
        'lastname' => true,
        'register_date' => true,
        'created_by' => true,
        'last_update' => true,
        'role' => true,
        'birthdate' => true,
        'module_participant' => true,
    ];
}
