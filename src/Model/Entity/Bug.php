<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bug Entity.
 */
class Bug extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'bugs_name' => true,
        'bugs_detail' => true,
        'modul_id' => true,
        'created_by' => true,
        'modified_by' => true,
        'status' => true,
        'priority' => true,
        'modul' => true,
    ];
}
