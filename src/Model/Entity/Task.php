<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Task Entity.
 */
class Task extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'task_name' => true,
        'taks' => true,
        'status' => true,
        'task_start' => true,
        'task_end' => true,
        'created_by' => true,
        'modified_by' => true,
        'modul_id' => true,
        'modul' => true,
    ];
}
