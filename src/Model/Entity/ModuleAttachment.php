<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ModuleAttachment Entity.
 */
class ModuleAttachment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'modul_id' => true,
        'path' => true,
        'ext' => true,
        'filename' => true,
        'upload_date' => true,
        'upload_by' => true,
        'modul' => true,
    ];
}
