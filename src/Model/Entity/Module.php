<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Module Entity.
 */
class Module extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'module_name' => true,
        'module_desc' => true,
        'project_id' => true,
        'created_by' => true,
        'modified_by' => true,
        'project' => true,
        'modul_comment' => true,
    ];
}
