<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Project Model
 */
class ProjectTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('project');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('Module', [
            'foreignKey' => 'project_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');
            
        $validator
            ->requirePresence('project_name', 'create')
            ->notEmpty('project_name');
            
        $validator
            ->requirePresence('project_desc', 'create')
            ->notEmpty('project_desc');
            
        $validator
            ->add('proejct_start', 'valid', ['rule' => 'datetime'])
            ->requirePresence('proejct_start', 'create')
            ->notEmpty('proejct_start');
            
        $validator
            ->add('project_end', 'valid', ['rule' => 'datetime'])
            ->requirePresence('project_end', 'create')
            ->notEmpty('project_end');
            
        $validator
            ->add('project_owner', 'valid', ['rule' => 'numeric'])
            ->requirePresence('project_owner', 'create')
            ->notEmpty('project_owner');
            
        $validator
            ->add('created_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('created_by', 'create')
            ->notEmpty('created_by');
            
        $validator
            ->add('updated_by', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('updated_by');

        return $validator;
    }
}
