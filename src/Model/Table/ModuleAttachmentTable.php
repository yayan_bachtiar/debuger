<?php
namespace App\Model\Table;

use App\Model\Entity\ModuleAttachment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ModuleAttachment Model
 */
class ModuleAttachmentTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('module_attachment');
        $this->belongsTo('Moduls', [
            'foreignKey' => 'modul_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->requirePresence('id', 'create')
            ->notEmpty('id');
            
        $validator
            ->requirePresence('path', 'create')
            ->notEmpty('path');
            
        $validator
            ->requirePresence('ext', 'create')
            ->notEmpty('ext');
            
        $validator
            ->requirePresence('filename', 'create')
            ->notEmpty('filename');
            
        $validator
            ->add('upload_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('upload_date', 'create')
            ->notEmpty('upload_date');
            
        $validator
            ->add('upload_by', 'valid', ['rule' => 'numeric'])
            ->requirePresence('upload_by', 'create')
            ->notEmpty('upload_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['modul_id'], 'Moduls'));
        return $rules;
    }
}
