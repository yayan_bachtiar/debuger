<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModulComment Controller
 *
 * @property \App\Model\Table\ModulCommentTable $ModulComment
 */
class ModulCommentController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Modules']
        ];
        $this->set('modulComment', $this->paginate($this->ModulComment));
        $this->set('_serialize', ['modulComment']);
    }

    /**
     * View method
     *
     * @param string|null $id Modul Comment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $modulComment = $this->ModulComment->get($id, [
            'contain' => ['Modules']
        ]);
        $this->set('modulComment', $modulComment);
        $this->set('_serialize', ['modulComment']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $modulComment = $this->ModulComment->newEntity();
        if ($this->request->is('post')) {
            $modulComment = $this->ModulComment->patchEntity($modulComment, $this->request->data);
            if ($this->ModulComment->save($modulComment)) {
                $this->Flash->success('The modul comment has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The modul comment could not be saved. Please, try again.');
            }
        }
        $modules = $this->ModulComment->Modules->find('list', ['limit' => 200]);
        $this->set(compact('modulComment', 'modules'));
        $this->set('_serialize', ['modulComment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Modul Comment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $modulComment = $this->ModulComment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $modulComment = $this->ModulComment->patchEntity($modulComment, $this->request->data);
            if ($this->ModulComment->save($modulComment)) {
                $this->Flash->success('The modul comment has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The modul comment could not be saved. Please, try again.');
            }
        }
        $modules = $this->ModulComment->Modules->find('list', ['limit' => 200]);
        $this->set(compact('modulComment', 'modules'));
        $this->set('_serialize', ['modulComment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Modul Comment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $modulComment = $this->ModulComment->get($id);
        if ($this->ModulComment->delete($modulComment)) {
            $this->Flash->success('The modul comment has been deleted.');
        } else {
            $this->Flash->error('The modul comment could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
