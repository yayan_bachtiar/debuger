<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModuleAttachment Controller
 *
 * @property \App\Model\Table\ModuleAttachmentTable $ModuleAttachment
 */
class ModuleAttachmentController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Moduls']
        ];
        $this->set('moduleAttachment', $this->paginate($this->ModuleAttachment));
        $this->set('_serialize', ['moduleAttachment']);
    }

    /**
     * View method
     *
     * @param string|null $id Module Attachment id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $moduleAttachment = $this->ModuleAttachment->get($id, [
            'contain' => ['Moduls']
        ]);
        $this->set('moduleAttachment', $moduleAttachment);
        $this->set('_serialize', ['moduleAttachment']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $moduleAttachment = $this->ModuleAttachment->newEntity();
        if ($this->request->is('post')) {
            $moduleAttachment = $this->ModuleAttachment->patchEntity($moduleAttachment, $this->request->data);
            if ($this->ModuleAttachment->save($moduleAttachment)) {
                $this->Flash->success('The module attachment has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module attachment could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleAttachment->Moduls->find('list', ['limit' => 200]);
        $this->set(compact('moduleAttachment', 'moduls'));
        $this->set('_serialize', ['moduleAttachment']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Module Attachment id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $moduleAttachment = $this->ModuleAttachment->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $moduleAttachment = $this->ModuleAttachment->patchEntity($moduleAttachment, $this->request->data);
            if ($this->ModuleAttachment->save($moduleAttachment)) {
                $this->Flash->success('The module attachment has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module attachment could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleAttachment->Moduls->find('list', ['limit' => 200]);
        $this->set(compact('moduleAttachment', 'moduls'));
        $this->set('_serialize', ['moduleAttachment']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Module Attachment id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $moduleAttachment = $this->ModuleAttachment->get($id);
        if ($this->ModuleAttachment->delete($moduleAttachment)) {
            $this->Flash->success('The module attachment has been deleted.');
        } else {
            $this->Flash->error('The module attachment could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
