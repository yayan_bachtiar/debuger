<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ModuleParticipant Controller
 *
 * @property \App\Model\Table\ModuleParticipantTable $ModuleParticipant
 */
class ModuleParticipantController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Moduls', 'Users']
        ];
        $this->set('moduleParticipant', $this->paginate($this->ModuleParticipant));
        $this->set('_serialize', ['moduleParticipant']);
    }

    /**
     * View method
     *
     * @param string|null $id Module Participant id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $moduleParticipant = $this->ModuleParticipant->get($id, [
            'contain' => ['Moduls', 'Users']
        ]);
        $this->set('moduleParticipant', $moduleParticipant);
        $this->set('_serialize', ['moduleParticipant']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $moduleParticipant = $this->ModuleParticipant->newEntity();
        if ($this->request->is('post')) {
            $moduleParticipant = $this->ModuleParticipant->patchEntity($moduleParticipant, $this->request->data);
            if ($this->ModuleParticipant->save($moduleParticipant)) {
                $this->Flash->success('The module participant has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module participant could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleParticipant->Moduls->find('list', ['limit' => 200]);
        $users = $this->ModuleParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('moduleParticipant', 'moduls', 'users'));
        $this->set('_serialize', ['moduleParticipant']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Module Participant id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $moduleParticipant = $this->ModuleParticipant->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $moduleParticipant = $this->ModuleParticipant->patchEntity($moduleParticipant, $this->request->data);
            if ($this->ModuleParticipant->save($moduleParticipant)) {
                $this->Flash->success('The module participant has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module participant could not be saved. Please, try again.');
            }
        }
        $moduls = $this->ModuleParticipant->Moduls->find('list', ['limit' => 200]);
        $users = $this->ModuleParticipant->Users->find('list', ['limit' => 200]);
        $this->set(compact('moduleParticipant', 'moduls', 'users'));
        $this->set('_serialize', ['moduleParticipant']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Module Participant id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $moduleParticipant = $this->ModuleParticipant->get($id);
        if ($this->ModuleParticipant->delete($moduleParticipant)) {
            $this->Flash->success('The module participant has been deleted.');
        } else {
            $this->Flash->error('The module participant could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
