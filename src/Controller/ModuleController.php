<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Module Controller
 *
 * @property \App\Model\Table\ModuleTable $Module
 */
class ModuleController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Projects']
        ];
        $this->set('module', $this->paginate($this->Module));
        $this->set('_serialize', ['module']);
    }

    /**
     * View method
     *
     * @param string|null $id Module id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $module = $this->Module->get($id, [
            'contain' => ['Projects', 'ModulComment']
        ]);
        $this->set('module', $module);
        $this->set('_serialize', ['module']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $module = $this->Module->newEntity();
        if ($this->request->is('post')) {
            $module = $this->Module->patchEntity($module, $this->request->data);
            if ($this->Module->save($module)) {
                $this->Flash->success('The module has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module could not be saved. Please, try again.');
            }
        }
        $projects = $this->Module->Projects->find('list', ['limit' => 200]);
        $this->set(compact('module', 'projects'));
        $this->set('_serialize', ['module']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Module id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $module = $this->Module->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $module = $this->Module->patchEntity($module, $this->request->data);
            if ($this->Module->save($module)) {
                $this->Flash->success('The module has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The module could not be saved. Please, try again.');
            }
        }
        $projects = $this->Module->Projects->find('list', ['limit' => 200]);
        $this->set(compact('module', 'projects'));
        $this->set('_serialize', ['module']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Module id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $module = $this->Module->get($id);
        if ($this->Module->delete($module)) {
            $this->Flash->success('The module has been deleted.');
        } else {
            $this->Flash->error('The module could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
