<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Project Controller
 *
 * @property \App\Model\Table\ProjectTable $Project
 */
class ProjectController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */

    public $paginate = [
        'limit'=>2
    ];

    public function index()
    {
        $this->set('project', $this->paginate($this->Project));
        $this->set('_serialize', ['project']);
    }

    /**
     * View method
     *
     * @param string|null $id Project id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $project = $this->Project->get($id, [
            'contain' => ['Module']
        ]);
        $this->set('project', $project);
        $this->set('_serialize', ['project']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $project = $this->Project->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['project_start']=date('Y-m-d', strtotime($this->request->data['project_start']));
            $this->request->data['project_end']=date('Y-m-d', strtotime($this->request->data['project_end']));
            $project = $this->Project->patchEntity($project, $this->request->data);
            if ($this->Project->save($project)) {
                $this->Flash->success('The project has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The project could not be saved. Please, try again.');
            }
        }
        $this->set(compact('project'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Project id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $project = $this->Project->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $project = $this->Project->patchEntity($project, $this->request->data);
            if ($this->Project->save($project)) {
                $this->Flash->success('The project has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The project could not be saved. Please, try again.');
            }
        }
        $this->set(compact('project'));
        $this->set('_serialize', ['project']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Project id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $project = $this->Project->get($id);
        if ($this->Project->delete($project)) {
            $this->Flash->success('The project has been deleted.');
        } else {
            $this->Flash->error('The project could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
