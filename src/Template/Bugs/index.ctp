<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New Bug'), ['action' => 'add']) ?></li>
    </ul>
</div>
<div class="bugs index large-10 medium-9 columns">
    <table cellpadding="0" cellspacing="0">
    <thead>
        <tr>
            <th><?= $this->Paginator->sort('id') ?></th>
            <th><?= $this->Paginator->sort('bugs_name') ?></th>
            <th><?= $this->Paginator->sort('modul_id') ?></th>
            <th><?= $this->Paginator->sort('created') ?></th>
            <th><?= $this->Paginator->sort('modified') ?></th>
            <th><?= $this->Paginator->sort('created_by') ?></th>
            <th><?= $this->Paginator->sort('modified_by') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($bugs as $bug): ?>
        <tr>
            <td><?= $this->Number->format($bug->id) ?></td>
            <td><?= h($bug->bugs_name) ?></td>
            <td><?= $this->Number->format($bug->modul_id) ?></td>
            <td><?= h($bug->created) ?></td>
            <td><?= h($bug->modified) ?></td>
            <td><?= $this->Number->format($bug->created_by) ?></td>
            <td><?= $this->Number->format($bug->modified_by) ?></td>
            <td class="actions">
                <?= $this->Html->link(__('View'), ['action' => 'view', $bug->id]) ?>
                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bug->id]) ?>
                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bug->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bug->id)]) ?>
            </td>
        </tr>

    <?php endforeach; ?>
    </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
