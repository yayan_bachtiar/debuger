<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $bug->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $bug->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Bugs'), ['action' => 'index']) ?></li>
    </ul>
</div>
<div class="bugs form large-10 medium-9 columns">
    <?= $this->Form->create($bug); ?>
    <fieldset>
        <legend><?= __('Edit Bug') ?></legend>
        <?php
            echo $this->Form->input('bugs_name');
            echo $this->Form->input('bugs_detail');
            echo $this->Form->input('modul_id');
            echo $this->Form->input('created_by');
            echo $this->Form->input('modified_by');
            echo $this->Form->input('status');
            echo $this->Form->input('priority');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
