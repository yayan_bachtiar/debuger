<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Bug'), ['action' => 'edit', $bug->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bug'), ['action' => 'delete', $bug->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bug->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bugs'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bug'), ['action' => 'add']) ?> </li>
    </ul>
</div>
<div class="bugs view large-10 medium-9 columns">
    <h2><?= h($bug->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Bugs Name') ?></h6>
            <p><?= h($bug->bugs_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($bug->id) ?></p>
            <h6 class="subheader"><?= __('Modul Id') ?></h6>
            <p><?= $this->Number->format($bug->modul_id) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($bug->created_by) ?></p>
            <h6 class="subheader"><?= __('Modified By') ?></h6>
            <p><?= $this->Number->format($bug->modified_by) ?></p>
            <h6 class="subheader"><?= __('Status') ?></h6>
            <p><?= $this->Number->format($bug->status) ?></p>
            <h6 class="subheader"><?= __('Priority') ?></h6>
            <p><?= $this->Number->format($bug->priority) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($bug->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($bug->modified) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Bugs Detail') ?></h6>
            <?= $this->Text->autoParagraph(h($bug->bugs_detail)); ?>

        </div>
    </div>
</div>
