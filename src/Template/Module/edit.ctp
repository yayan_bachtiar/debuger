<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $module->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $module->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Module'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Modul Comment'), ['controller' => 'ModulComment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Modul Comment'), ['controller' => 'ModulComment', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="module form large-10 medium-9 columns">
    <?= $this->Form->create($module); ?>
    <fieldset>
        <legend><?= __('Edit Module') ?></legend>
        <?php
            echo $this->Form->input('module_name');
            echo $this->Form->input('module_desc');
            echo $this->Form->input('project_id');
            echo $this->Form->input('created_by');
            echo $this->Form->input('modified_by');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
