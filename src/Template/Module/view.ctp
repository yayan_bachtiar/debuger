<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Module'), ['action' => 'edit', $module->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Module'), ['action' => 'delete', $module->id], ['confirm' => __('Are you sure you want to delete # {0}?', $module->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Modul Comment'), ['controller' => 'ModulComment', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Modul Comment'), ['controller' => 'ModulComment', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="module view large-10 medium-9 columns">
    <h2><?= h($module->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Module Name') ?></h6>
            <p><?= h($module->module_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($module->id) ?></p>
            <h6 class="subheader"><?= __('Project Id') ?></h6>
            <p><?= $this->Number->format($module->project_id) ?></p>
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= $this->Number->format($module->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= $this->Number->format($module->modified) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($module->created_by) ?></p>
            <h6 class="subheader"><?= __('Modified By') ?></h6>
            <p><?= $this->Number->format($module->modified_by) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Module Desc') ?></h6>
            <?= $this->Text->autoParagraph(h($module->module_desc)); ?>

        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related ModulComment') ?></h4>
    <?php if (!empty($module->modul_comment)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Comment') ?></th>
            <th><?= __('Module Id') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Created By') ?></th>
            <th><?= __('Modified') ?></th>
            <th><?= __('Modified By') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($module->modul_comment as $modulComment): ?>
        <tr>
            <td><?= h($modulComment->id) ?></td>
            <td><?= h($modulComment->comment) ?></td>
            <td><?= h($modulComment->module_id) ?></td>
            <td><?= h($modulComment->created) ?></td>
            <td><?= h($modulComment->created_by) ?></td>
            <td><?= h($modulComment->modified) ?></td>
            <td><?= h($modulComment->modified_by) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'ModulComment', 'action' => 'view', $modulComment->]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'ModulComment', 'action' => 'edit', $modulComment->]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ModulComment', 'action' => 'delete', $modulComment->], ['confirm' => __('Are you sure you want to delete # {0}?', $modulComment->)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
