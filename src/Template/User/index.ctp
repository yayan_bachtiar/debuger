<div class="container">
    <div class="row">
        <div class="col m12 l12">

            <table>
                <thead>
                    <tr>
                        <th>No</th>
                        <th><?= $this->Paginator->sort('username') ?></th>
                        <th><?= $this->Paginator->sort('register_date') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1; foreach ($user as $user): ?>
                    <tr>
                        <td><?= $i++; ?></td>
                        <td><?= h($user->username) ?></td>
                        <td><?= h($user->register_date) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $user->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                        </td>
                    </tr>

                    <?php endforeach; ?>
                </tbody>
            </table>
            <ul class="pagination">
                <?= $this->Paginator->prev('<i class="mdi-navigation-chevron-left"></i> ', ['escape'=>false]) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next('<i class="mdi-navigation-chevron-right"></i>', ['escape'=>false]) ?>
            </ul>
            <p><?= $this->Paginator->counter() ?></p>
            <br>
        </div>
    </div>
</div>
<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="user index large-10 medium-9 columns">
    <div class="">

    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><a class="btn-floating red"><i class="large mdi-editor-insert-chart"></i></a></li>
        <li><a class="btn-floating yellow darken-1"><i class="large mdi-editor-format-quote"></i></a></li>
        <li><a class="btn-floating green"><i class="large mdi-editor-publish"></i></a></li>
        <li><a class="btn-floating blue"><i class="large mdi-editor-attach-file"></i></a></li>
    </ul>
</div>
