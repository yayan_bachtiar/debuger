<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List User'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="user view large-10 medium-9 columns">
    <h2><?= h($user->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Username') ?></h6>
            <p><?= h($user->username) ?></p>
            <h6 class="subheader"><?= __('Password') ?></h6>
            <p><?= h($user->password) ?></p>
            <h6 class="subheader"><?= __('Firstname') ?></h6>
            <p><?= h($user->firstname) ?></p>
            <h6 class="subheader"><?= __('Lastname') ?></h6>
            <p><?= h($user->lastname) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($user->id) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($user->created_by) ?></p>
            <h6 class="subheader"><?= __('Role') ?></h6>
            <p><?= $this->Number->format($user->role) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Register Date') ?></h6>
            <p><?= h($user->register_date) ?></p>
            <h6 class="subheader"><?= __('Last Update') ?></h6>
            <p><?= h($user->last_update) ?></p>
            <h6 class="subheader"><?= __('Birthdate') ?></h6>
            <p><?= h($user->birthdate) ?></p>
        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related ModuleParticipant') ?></h4>
    <?php if (!empty($user->module_participant)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Modul Id') ?></th>
            <th><?= __('User Id') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($user->module_participant as $moduleParticipant): ?>
        <tr>
            <td><?= h($moduleParticipant->id) ?></td>
            <td><?= h($moduleParticipant->modul_id) ?></td>
            <td><?= h($moduleParticipant->user_id) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'ModuleParticipant', 'action' => 'view', $moduleParticipant->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'ModuleParticipant', 'action' => 'edit', $moduleParticipant->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ModuleParticipant', 'action' => 'delete', $moduleParticipant->id], ['confirm' => __('Are you sure you want to delete # {0}?', $moduleParticipant->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
