<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List User'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module Participant'), ['controller' => 'ModuleParticipant', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="user form large-10 medium-9 columns">
    <?= $this->Form->create($user); ?>
    <fieldset>
        <legend><?= __('Edit User') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('register_date');
            echo $this->Form->input('created_by');
            echo $this->Form->input('last_update');
            echo $this->Form->input('role');
            echo $this->Form->input('birthdate');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
