<div class="container">
    <div class="row">
        <div class="col l12">
            <div class="project index large-10 medium-9 columns">
                <div class="row">
                    <?php foreach($project as $project):?>
                    <div class="col l6 s12">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <?= $this->Html->image('office.jpg');?>
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Card Title <i class="mdi-navigation-more-vert right"></i></span>
                                <p><a href="#">This is a link</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Card Title <i class="mdi-navigation-close right"></i></span>
                                <p>Here is some more information about this product that is only revealed once clicked on.</p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                    <div class="col l6 s12">
                        <div class="card">
                            <div class="card-image waves-effect waves-block waves-light">
                                <?= $this->Html->image('parallax1.jpg');?>
                            </div>
                            <div class="card-content">
                                <span class="card-title activator grey-text text-darken-4">Card Title <i class="mdi-navigation-more-vert right"></i></span>
                                <p><a href="#">This is a link</a></p>
                            </div>
                            <div class="card-reveal">
                                <span class="card-title grey-text text-darken-4">Card Title <i class="mdi-navigation-close right"></i></span>
                                <p>Here is some more information about this product that is only revealed once clicked on.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <table cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?= $this->Paginator->sort('id') ?></th>
                            <th><?= $this->Paginator->sort('project_name') ?></th>
                            <th><?= $this->Paginator->sort('proejct_start') ?></th>
                            <th><?= $this->Paginator->sort('project_end') ?></th>
                            <th><?= $this->Paginator->sort('project_owner') ?></th>
                            <th><?= $this->Paginator->sort('created') ?></th>
                            <th><?= $this->Paginator->sort('modified') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($project as $project): ?>
                        <tr>
                            <td><?= $this->Number->format($project->id) ?></td>
                            <td><?= h($project->project_name) ?></td>
                            <td><?= h($project->proejct_start) ?></td>
                            <td><?= h($project->project_end) ?></td>
                            <td><?= $this->Number->format($project->project_owner) ?></td>
                            <td><?= h($project->created) ?></td>
                            <td><?= h($project->modified) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['action' => 'view', $project->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['action' => 'edit', $project->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $project->id], ['confirm' => __('Are you sure you want to delete # {0}?', $project->id)]) ?>
                            </td>
                        </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="">
                    <ul class="pagination">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p><?= $this->Paginator->counter() ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a class="btn-floating btn-large red">
        <i class="large mdi-editor-mode-edit"></i>
    </a>
    <ul>
        <li><?php echo $this->Html->link('<i class="large mdi-av-my-library-add"></i>', ['controller'=>'project', 'action'=>'add'], ['style'=>"transform: scaleY(0.4) scaleX(0.4) translateY(40px); opacity: 0;", 'class'=>'btn-floating red darken-1 tooltipped', 'data-tooltip'=>'Add Project', 'data-position'=>'left', 'data-delay'=>50, 'escape'=>false]);?></li>
    </ul>
</div>
