<div class="container">
    <div class="row">
        <div class="col s12 l9">
            <div class="project form large-10 medium-9 columns">
                <?php
$this->loadHelper('Form', [
    'templates' => 'app_form',
]);
echo $this->Form->create($project, ['type'=>'file']);
                ?>
                <?php
echo $this->Form->input('project_name', ['length'=>50, 'autocomplete'=>'off']);
echo $this->Form->input('project_desc', ['class'=>'materialize-textarea', 'length'=>255]);?>
                <div class="input-field col s6">
                    <input id="project_start" name="project_start" type="date" class="datepicker" autocomplete='off'>
                    <label for="project_start">Start Project</label>
                </div>
                <div class="input-field col s6">
                    <input id="project_end" name="project_end" type="date" class="datepicker" autocomplete='off'>
                    <label for="project_end">End Project</label>
                </div>
                <div class="input-field col s12">
                    <select>
                        <option value="" disabled selected>Choose your option</option>
                        <option value="1">Option 1</option>
                        <option value="2">Option 2</option>
                        <option value="3">Option 3</option>
                    </select>
                    <label>Project Owner</label>
                </div>
                <div class="file-field input-field col s12">
                    <input class="file-path validate" type="text" />
                    <div class="btn">
                        <span>File</span>
                        <input type="file" name="project_image"/>
                    </div>
                </div>
                <button class="btn waves-effect waves-light" type="submit" name="action">Submit
                    <i class="mdi-content-send right"></i>
                </button>
                <?= $this->Form->end() ?>
            </div>

        </div>
        <div class="col s12 l3 hide-on-small-only"></div>
    </div>
</div>
