<div class="actions columns large-2 medium-3">
    <h3><?= __('Actions') ?></h3>
    <ul class="side-nav">
        <li><?= $this->Html->link(__('Edit Project'), ['action' => 'edit', $project->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Project'), ['action' => 'delete', $project->id], ['confirm' => __('Are you sure you want to delete # {0}?', $project->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Project'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Project'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Module'), ['controller' => 'Module', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Module'), ['controller' => 'Module', 'action' => 'add']) ?> </li>
    </ul>
</div>
<div class="project view large-10 medium-9 columns">
    <h2><?= h($project->id) ?></h2>
    <div class="row">
        <div class="large-5 columns strings">
            <h6 class="subheader"><?= __('Project Name') ?></h6>
            <p><?= h($project->project_name) ?></p>
        </div>
        <div class="large-2 columns numbers end">
            <h6 class="subheader"><?= __('Id') ?></h6>
            <p><?= $this->Number->format($project->id) ?></p>
            <h6 class="subheader"><?= __('Project Owner') ?></h6>
            <p><?= $this->Number->format($project->project_owner) ?></p>
            <h6 class="subheader"><?= __('Created By') ?></h6>
            <p><?= $this->Number->format($project->created_by) ?></p>
            <h6 class="subheader"><?= __('Updated By') ?></h6>
            <p><?= $this->Number->format($project->updated_by) ?></p>
        </div>
        <div class="large-2 columns dates end">
            <h6 class="subheader"><?= __('Proejct Start') ?></h6>
            <p><?= h($project->proejct_start) ?></p>
            <h6 class="subheader"><?= __('Project End') ?></h6>
            <p><?= h($project->project_end) ?></p>
            <h6 class="subheader"><?= __('Created') ?></h6>
            <p><?= h($project->created) ?></p>
            <h6 class="subheader"><?= __('Modified') ?></h6>
            <p><?= h($project->modified) ?></p>
        </div>
    </div>
    <div class="row texts">
        <div class="columns large-9">
            <h6 class="subheader"><?= __('Project Desc') ?></h6>
            <?= $this->Text->autoParagraph(h($project->project_desc)); ?>

        </div>
    </div>
</div>
<div class="related row">
    <div class="column large-12">
    <h4 class="subheader"><?= __('Related Module') ?></h4>
    <?php if (!empty($project->module)): ?>
    <table cellpadding="0" cellspacing="0">
        <tr>
            <th><?= __('Id') ?></th>
            <th><?= __('Module Name') ?></th>
            <th><?= __('Module Desc') ?></th>
            <th><?= __('Project Id') ?></th>
            <th><?= __('Created') ?></th>
            <th><?= __('Modified') ?></th>
            <th><?= __('Created By') ?></th>
            <th><?= __('Modified By') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        <?php foreach ($project->module as $module): ?>
        <tr>
            <td><?= h($module->id) ?></td>
            <td><?= h($module->module_name) ?></td>
            <td><?= h($module->module_desc) ?></td>
            <td><?= h($module->project_id) ?></td>
            <td><?= h($module->created) ?></td>
            <td><?= h($module->modified) ?></td>
            <td><?= h($module->created_by) ?></td>
            <td><?= h($module->modified_by) ?></td>

            <td class="actions">
                <?= $this->Html->link(__('View'), ['controller' => 'Module', 'action' => 'view', $module->id]) ?>

                <?= $this->Html->link(__('Edit'), ['controller' => 'Module', 'action' => 'edit', $module->id]) ?>

                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Module', 'action' => 'delete', $module->id], ['confirm' => __('Are you sure you want to delete # {0}?', $module->id)]) ?>

            </td>
        </tr>

        <?php endforeach; ?>
    </table>
    <?php endif; ?>
    </div>
</div>
