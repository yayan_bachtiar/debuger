<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="msapplication-tap-highlight" content="no">
        <meta name="description" content="">
        <!-- Favicons-->
        <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <link rel="icon" href="img/favicon/favicon-32x32.png" sizes="32x32">
        <!--  Android 5 Chrome Color-->
        <meta name="theme-color" content="#EE6E73">
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $cakeDescription ?>
        </title>
        <?= $this->Html->css('prism.css') ?>
        <?= $this->Html->css('ghpages-materialize.css'); ?>
        <style>
            body {
                display: flex;
                min-height: 100vh;
                flex-direction: column;
            }

            main {
                flex: 1 0 auto;
            }
        </style>
    </head>
    <body class="home">
        <header>
            <nav class="top-nav">
                <div class="container">
                    <div class="nav-wrapper"><a class="page-title"><?= $this->fetch('title')?></a></div>
                </div>
            </nav>
            <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle"><i class="mdi-navigation-menu"></i></a></div>
            <ul id="nav-mobile" class="side-nav fixed">
                <li class="logo"><a id="logo-container" href="http://materializecss.com/" class="brand-logo">
                    <object id="front-page-logo" type="image/svg+xml" data="res/materialize.svg">Your browser does not support SVG</object></a></li>
                <li class="bold"><?= $this->Html->link('Home', ['controller'=>'pages', 'action'=>'home'], ['class'=>'waves-effect wave-teal','escape'=>false]);?></li>
                <li class="bold"><?= $this->Html->link('My Porject', ['controller'=>'project', 'action'=>'index'], ['class'=>'waces-effect wave-teal', 'escape'=>false]);?></li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-teal">Issues</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Issues',['controller'=>'bugs', 'action'=>'my_issue'])?></li>
                                    <li><?=$this->Html->link('All Issues',['controller'=>'bugs', 'action'=>'index'])?></li>
                                    <li><?=$this->Html->link('Open Issues',['controller'=>'bugs', 'action'=>'open_issue'])?></li>
                                    <li><?=$this->Html->link('Close Issues',['controller'=>'bugs', 'action'=>'closed_issue'])?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">Debuger</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Test', ['controller'=>'Bugs', 'action'=>'my_test'])?></li>
                                    <li><?=$this->Html->link('All Test', ['controller'=>'Bugs', 'action'=>'all_test'])?></li>
                                    <li><?=$this->Html->link('Resolved Bugs', ['controller'=>'Bugs', 'action'=>'resolved'])?></li>
                                    <li><?=$this->Html->link('Bugs Issue', ['controller'=>'Bugs', 'action'=>'bugs_issue'])?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">Account</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('My Account', ['controller'=>'user', 'action', 'view'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Edit My Account', ['controller'=>'user', 'action', 'edit'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('My Password', ['controller'=>'user', 'action', 'edit_password'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Log Out', ['controller'=>'user', 'action', 'logout'], ['escape'=>false]);?></li>
                                </ul>
                            </div>
                        </li>
                        <li class="bold"><a class="collapsible-header  waves-effect waves-teal">System</a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><?=$this->Html->link('User', ['controller'=>'user', 'action', 'all_user'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Role', ['controller'=>'Role', 'action', 'index'], ['escape'=>false]);?></li>
                                    <li><?=$this->Html->link('Status', ['controller'=>'status', 'action', 'index'], ['escape'=>false]);?></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="bold"><a href="showcase.html" class="waves-effect waves-teal">Showcase</a></li>
            </ul>
        </header>
        <main>
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </main>
        <footer class="page-footer">
            <div class="container">
                <div class="row">
                    <div class="col l4 s12">
                        <h5 class="white-text">Help Materialize Grow</h5>
                        <p class="grey-text text-lighten-4">We are a team of college students working on this project like it's our full time job. Any amount would help support and continue development on this project and is greatly appreciated.</p>
                    </div>
                    <div class="col l4 s12">
                        <h5 class="white-text">Join the Discussion</h5>
                        <p class="grey-text text-lighten-4">We have a Gitter chat room set up where you can talk directly with us. Come in and discuss new features, future goals, general problems or questions, or anything else you can think of.</p>
                        <a class="btn waves-effect waves-light red lighten-3" target="_blank" href="https://gitter.im/Dogfalo/materialize">Chat</a>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    © 2014-2015 Materialize, All rights reserved.
                    <a class="grey-text text-lighten-4 right" href="https://github.com/Dogfalo/materialize/blob/master/LICENSE">MIT License</a>
                </div>
            </div>
        </footer>
        <?= $this->Html->script('jquery-2.1.1.min.js');?>
        <?= $this->Html->script('jquery.timeago.min.js');?>
        <?= $this->Html->script('prism.js');?>
        <?= $this->Html->script('materialize.js');?>
        <?= $this->Html->script('init.js');?>
    </body>
</html>
